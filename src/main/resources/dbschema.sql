DROP DATABASE IF EXISTS `plazoleta`;
CREATE DATABASE  IF NOT EXISTS `plazoleta`;

--
-- Table restaurant
--

DROP TABLE IF EXISTS `plazoleta`.`restaurant`;
CREATE TABLE `plazoleta`.`restaurant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `id_owner` bigint(20) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `url_logo` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- table dish
--

DROP TABLE IF EXISTS `plazoleta`.`dish`;
CREATE TABLE `plazoleta`.`dish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `description` varchar(45) NOT NULL,
  `price` bigint(20) NOT NULL,
  `id_restaurant` bigint(20) NOT NULL,
  `url_image` varchar(45) NOT NULL,
  `active` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `plazoleta`.`dish`
ADD CONSTRAINT `fk_dish_restaurants`
  FOREIGN KEY (`id_restaurant`)
  REFERENCES `plazoleta`.`restaurant` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

--
-- table order
--

DROP TABLE IF EXISTS `plazoleta`.`orders`;
CREATE TABLE `plazoleta`.`orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_client` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(45) NOT NULL,
  `id_chef` bigint(20) NOT NULL,
  `id_restaurant` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- table orderDish
--

DROP TABLE IF EXISTS `plazoleta`.`orderDish`;
CREATE TABLE `plazoleta`.`orderDish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_order` bigint(20) NOT NULL,
  `id_dish` bigint(20) NOT NULL,
  `quantity` date NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `plazoleta`.`orderDish`
ADD CONSTRAINT `fk_order_orderDish`
  FOREIGN KEY (`id_order`)
  REFERENCES `plazoleta`.`orders` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `plazoleta`.`orderDish`
ADD CONSTRAINT `fk_dish_orderDish`
  FOREIGN KEY (`id_dish`)
  REFERENCES `plazoleta`.`dish` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



