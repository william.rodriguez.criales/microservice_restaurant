package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.impl;


import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishUpdateRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.DishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.IDishHandler;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IDishRequestMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IDishResponseMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IDishUpdateRequestMapper;
import com.pragma.powerup.restaurantmicroservice.domain.api.IDishServicePort;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DishHandlerImpl implements IDishHandler {
    private final IDishServicePort dishServicePort;
    private final IDishRequestMapper dishRequestMapper;
    private final IDishResponseMapper dishResponseMapper;
    //private final DishRequestDto dishRequestDto;
    private final IDishUpdateRequestMapper dishUpdateRequestMapper;

    @Override
    public DishResponseDto saveDish(DishRequestDto dishRequestDto) {
        Dish dish = dishServicePort.saveDish(dishRequestMapper.toDish(dishRequestDto));
        return dishResponseMapper.toDishResponseDto(dish);
    }

    @Override
    public DishResponseDto updateDish(DishUpdateRequestDto dishUpdateRequestDto, Long id) {
        Dish dish = dishServicePort.updateDish(dishUpdateRequestMapper.toDish(dishUpdateRequestDto, id ));
        return  dishResponseMapper.toDishResponseDto(dish);
    }

    @Override
    public DishResponseDto activeDish(Long id) {
        Dish dish = dishServicePort.activeDish(id);
        return dishResponseMapper.toDishResponseDto(dish);
    }

    @Override
    public Page<DishResponseDto> getAllDish(Pageable pageable, String idCategory) {
        Page<Dish> allDish = dishServicePort.getAllDish(pageable, idCategory);
        return allDish.map(dish -> dishResponseMapper.toDishResponseDto(dish));
    }

}
