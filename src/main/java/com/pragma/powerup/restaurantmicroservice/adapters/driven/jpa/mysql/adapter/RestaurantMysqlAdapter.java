package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.exceptions.RestaurantNotFoundException;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.mappers.IRestaurantEntityMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.repositories.IRestaurantRepository;
import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IRestaurantPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@RequiredArgsConstructor
public class RestaurantMysqlAdapter implements IRestaurantPersistencePort {
    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;

    @Override
    public Restaurant saveRestaurant (Restaurant restaurant) {

        RestaurantEntity restaurantEntity = restaurantRepository.saveAndFlush(restaurantEntityMapper.toEntity(restaurant));
        return restaurantEntityMapper.toRestaurant(restaurantEntity);
    }

    @Override
    public Restaurant getById(Long id) {

        Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(id);

        if (restaurantEntity.isEmpty()) {
            throw new RestaurantNotFoundException();
        }

        return restaurantEntityMapper.toRestaurant(restaurantEntity.get());
    }

    @Override
    public Page<Restaurant> getAllRestaurant(Pageable pageable){
        Page<RestaurantEntity> restaurantEntities = restaurantRepository.findAll(pageable);
        return restaurantEntities.map(restaurantEntityMapper::toRestaurant);
    }





}
