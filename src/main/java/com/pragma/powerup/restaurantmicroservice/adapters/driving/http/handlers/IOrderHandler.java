package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DeliverOrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.OrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderDishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IOrderHandler {
    OrderResponseDto makeOrder(OrderRequestDto orderRequestDto);

    Page<OrderDishResponseDto> getAllOrders(Pageable pageable, String status);


    OrderDishResponseDto assignOrder(Long id);

    OrderDishResponseDto notifyOrder(Long id);

    OrderDishResponseDto deliverOrder(Long idOrder, DeliverOrderRequestDto deliverOrderRequestDto);

    void cancelOrder(Long idOrder);
}
