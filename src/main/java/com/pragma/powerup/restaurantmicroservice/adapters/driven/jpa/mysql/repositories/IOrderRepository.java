package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IOrderRepository extends JpaRepository<OrderEntity, Long> {


    Optional<OrderEntity> findByIdClient(Long idClient);

    Optional<OrderEntity> findByStatus(String pendiente);

    Optional<OrderEntity> findBystatus(String pendiente);

    Optional<OrderEntity> findAllByIdClient(Long idClient);

    //@Query("select * from OrderEntity where idClient=:idClient and status in (:status)")
    List<OrderEntity> findAllByIdClientAndStatusIn(Long idClient, List<String> status);

    Page<OrderEntity> findAllByStatus(Pageable pageable, String status);
}
