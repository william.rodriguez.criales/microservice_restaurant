package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.DishResponseDto;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IDishResponseMapper {

    DishResponseDto toDishResponseDto(Dish dish);
/*
    @Mapping(source = "person.name", target = "name")
    @Mapping(source = "person.surname", target = "surname")
    @Mapping(source = "person.mail", target = "mail")
    @Mapping(source = "person.phone", target = "phone")
    @Mapping(source = "person.address", target = "address")
    @Mapping(source = "person.idDniType", target = "idDniType")
    @Mapping(source = "person.dniNumber", target = "dniNumber")
    @Mapping(source = "person.idPersonType", target = "idPersonType")
    PersonResponseDto userToPersonResponse(User user);
    List<PersonResponseDto> userListToPersonResponseList(List<User> userList);


    PersonResponseDto toPersonResponseDto(Person person);
*/
}


