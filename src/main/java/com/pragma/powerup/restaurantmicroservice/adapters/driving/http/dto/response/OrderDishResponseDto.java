package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.OrderDishRequestDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Getter
public class OrderDishResponseDto {

    private Long idRestaurant;
    private Long id;
    private Long idClient;
    private LocalDateTime date;
    private String status;
    private Long idChef;
    private List<OrderDishRequestDto> orderDishes;
}
