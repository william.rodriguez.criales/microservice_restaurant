package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DeliverOrderRequestDto {

    private String pin;
    private String status;

}
