package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishUpdateRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.DishResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IDishHandler {
    DishResponseDto saveDish (DishRequestDto dishRequestDto);
    DishResponseDto updateDish (DishUpdateRequestDto dishUpdateRequestDto, Long id);

    DishResponseDto activeDish(Long id);

    Page<DishResponseDto> getAllDish(Pageable pageable, String idCategory);
}
