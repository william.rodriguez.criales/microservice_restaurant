package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "order_dish")
//@NoArgsConstructor
//@AllArgsConstructor
@Data
public class OrderDishEntity {


    //@ManyToOne
    //@JoinColumn(name="idOrder", insertable = false, updatable = false)
    //private OrderEntity orderEntity;

    //public OrderDishEntity() {}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //@Column(name = "id_order")
    //private Long idOrder;
    private Long idDish;
    private Long quantity;

}
