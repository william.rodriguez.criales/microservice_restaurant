package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.controller;


import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DishUpdateRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.DishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.IDishHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dish")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class DishRestController {
    private final IDishHandler dishHandler;

    @Operation(summary = "Add a new dish",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Dish created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurant already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})

    @PostMapping
    public ResponseEntity<DishResponseDto> saveDish(@RequestBody DishRequestDto dishRequestDto) {
        DishResponseDto dishResponseDto = dishHandler.saveDish(dishRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(dishResponseDto);
    }

    @Operation(summary = "update price and description dish",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Dish update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurant already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})

    @PatchMapping("/{id}")
    public ResponseEntity<DishResponseDto> updateDish(@RequestBody DishUpdateRequestDto dishUpdateRequestDto, @PathVariable Long id) {
        DishResponseDto dishResponseDto = dishHandler.updateDish(dishUpdateRequestDto, id);
        return ResponseEntity.status(HttpStatus.OK).body(dishResponseDto);
    }

    @Operation(summary = "activate and disable dish",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Dish update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurant already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})

    @PatchMapping("/activate/{id}")
    public ResponseEntity<DishResponseDto> upDownDish(@PathVariable Long id) {
        DishResponseDto dishResponseDto = dishHandler.activeDish(id);
        return ResponseEntity.status(HttpStatus.OK).body(dishResponseDto);
    }

    @Operation(summary = "List Dish")

    @GetMapping("/{id}")
    public ResponseEntity<Page<DishResponseDto>> getAllDish(@RequestParam(required = false) String idCategory, @ParameterObject Pageable pageable) {

        Page<DishResponseDto> dishResponseDto = dishHandler.getAllDish(pageable, idCategory);
        return ResponseEntity.status(HttpStatus.OK)
                .body(dishResponseDto);
    }
}
