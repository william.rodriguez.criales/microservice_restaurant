package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.exceptions.OrderNotFoundException;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.mappers.IOrderEntityMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.repositories.IOrderDishRepository;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.repositories.IOrderRepository;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IOrderPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class OrderMysqlAdapter implements IOrderPersistencePort {

    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;
    private final IOrderDishRepository orderDishRepository;

   // @Override
    public Order makeOrder(Order order){

        OrderEntity orderEntity = orderRepository.saveAndFlush(orderEntityMapper.toOrderEntity(order));
        return  orderEntityMapper.toOrder(orderEntity);

    }

    @Override
    public List<Order> findByIdClientAndStatus(Long idClient, List<String> status) {


        List<OrderEntity> orderEntityList = orderRepository.findAllByIdClientAndStatusIn(idClient,status);

        return orderEntityList.stream().map(orderEntityMapper::toOrder).toList();

    }

    @Override
    public Page<Order> getAllOrders(Pageable pageable){
        Page<OrderEntity> orderEntityPage = orderRepository.findAll(pageable);
        return orderEntityPage.map(orderEntityMapper::toOrder);

    }

    @Override
    public Page<Order> getAllOrders(Pageable pageable, String status) {
        Page<OrderEntity> orderEntityPage = orderRepository.findAllByStatus(pageable, status);
        return orderEntityPage.map(orderEntityMapper::toOrder);

    }

    @Override
    public Order assignOrder(Order order, Long idChef) {
        order.setIdChef(idChef);
        order.setStatus("en_preparacion");
        OrderEntity orderEntity = orderRepository.saveAndFlush(orderEntityMapper.toOrderEntity(order));
        return orderEntityMapper.toOrder(orderEntity);
    }

    @Override
    public Order notifyOrder(Order order) {

        order.setStatus("listo");
        OrderEntity orderEntity = orderRepository.saveAndFlush(orderEntityMapper.toOrderEntity(order));
        return orderEntityMapper.toOrder(orderEntity);

    }

/*
    @Override
    public Order deliverOrder(Order order) {

        order.setStatus("entregado");
        OrderEntity orderEntity = orderRepository.saveAndFlush(orderEntityMapper.toOrderEntity(order));
        return orderEntityMapper.toOrder(orderEntity);

    }

 */


    @Override
    public  Order deliverOrder(Order order, String status){
        order.setStatus(status);
        OrderEntity orderEntity = orderRepository.saveAndFlush(orderEntityMapper.toOrderEntity(order));
        return orderEntityMapper.toOrder(orderEntity);
    }

    @Override
    public Order getById(Long id) {

        Optional<OrderEntity> orderEntity = orderRepository.findById(id);
        if (orderEntity.isEmpty()) {
            throw new OrderNotFoundException();
        }
        return orderEntityMapper.toOrder(orderEntity.get());
    }

    @Override
    public void cancelOrder(Long idOrder){
        orderRepository.deleteById(idOrder);

    }

}
