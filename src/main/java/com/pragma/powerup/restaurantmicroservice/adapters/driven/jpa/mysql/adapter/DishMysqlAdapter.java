package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.exceptions.DishNotFoundException;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.mappers.IDishEntityMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.repositories.IDishRepository;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IDishPersistencePort;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@RequiredArgsConstructor
public class DishMysqlAdapter implements IDishPersistencePort {
    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;


    @Override
    public Dish saveDish(Dish dish) {

        DishEntity dishEntity = dishRepository.saveAndFlush(dishEntityMapper.toEntity(dish));
        return dishEntityMapper.toDish(dishEntity);
    }
/*
    @Override
    public Dish updateDish(Dish dish, Long id){
        DishEntity dishEntity = dishRepository.saveAndFlush(dishEntityMapper.toEntity(dish.setPrice(dish.getPrice(id)));
        //DishEntity dishEntity = dishRepository.saveAndFlush(dishEntityMapper.toEntity(dish));
        return dishEntityMapper.toDish(dishEntity);
    }

 */

    @Override
    public Dish updateDish(Dish dish) {
        Dish dishToUpdate = getById(dish.getId());
        dishToUpdate.setDescription(dish.getDescription());
        dishToUpdate.setPrice(dish.getPrice());
        DishEntity dishEntity = dishRepository.saveAndFlush(dishEntityMapper.toEntity(dishToUpdate));
        return dishEntityMapper.toDish(dishEntity);
    }


    @SneakyThrows
    @Override
    public Dish getById(Long idDish) {

        Optional<DishEntity> dishEntity = dishRepository.findById(idDish);

        if (dishEntity.isEmpty()) {
            throw new DishNotFoundException();
        }

        return dishEntityMapper.toDish(dishEntity.get());
    }

    @Override
    public Dish activeDish(Dish dish) {
        dish.setActive(!dish.getActive());
        DishEntity dishEntity = dishRepository.saveAndFlush(dishEntityMapper.toEntity(dish));
        return dishEntityMapper.toDish(dishEntity);
    }

    @Override
    public Page<Dish> getAllDish(Pageable pageable, String idCategory) {
        Page<DishEntity> dishEntity = dishRepository.findByIdCategory(idCategory, pageable);
        return dishEntity.map(dishEntityMapper::toDish);
    }

    @Override
    public Page<Dish> getAllDish(Pageable pageable) {
        Page<DishEntity> dishEntity = dishRepository.findAll(pageable);
        return dishEntity.map(dishEntityMapper::toDish);


    }
}
