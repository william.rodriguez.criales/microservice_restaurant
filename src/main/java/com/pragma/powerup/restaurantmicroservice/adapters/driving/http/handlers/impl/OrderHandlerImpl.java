package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DeliverOrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.OrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderDishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.IOrderHandler;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IDeliverOrderRequestMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IOrderRequestMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IOrderResponseMapper;
import com.pragma.powerup.restaurantmicroservice.domain.api.IOrderServicePort;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderHandlerImpl implements IOrderHandler {
    private final IOrderServicePort orderServicePort;
    private final IOrderRequestMapper orderRequestMapper;
    private final IOrderResponseMapper orderResponseMapper;
    private final IDeliverOrderRequestMapper deliverOrderRequestMapper;

    @Override
    public OrderResponseDto makeOrder(OrderRequestDto orderRequestDto) {
        Order order = orderServicePort.makeOrder(orderRequestMapper.toOrder(orderRequestDto));
        return orderResponseMapper.toOrderResponseDto(order);
    }

    @Override
    public Page<OrderDishResponseDto> getAllOrders(Pageable pageable, String status){
        Page<Order> orderPage = orderServicePort.getAllOrders(pageable, status);
        return orderPage.map(orderResponseMapper::toOrderDishResponseDto);
    }


    @Override
    public OrderDishResponseDto assignOrder(Long id) {
        Order order = orderServicePort.assignOrder(id);
        return orderResponseMapper.toOrderDishResponseDto(order);
    }

    @Override
    public OrderDishResponseDto notifyOrder(Long id) {
        Order order = orderServicePort.notifyOrder(id);
        return orderResponseMapper.toOrderDishResponseDto(order);
    }

    @Override
    public  OrderDishResponseDto deliverOrder(Long idOrder, DeliverOrderRequestDto deliverOrderRequestDto){
        Order order = orderServicePort.deliverOrder(deliverOrderRequestMapper.toOrder(idOrder, deliverOrderRequestDto));
        return  orderResponseMapper.toOrderDishResponseDto(order);

    }

    @Override
    public void cancelOrder(Long idOrder) {

        orderServicePort.cancelOrder(idOrder);
    }
}
