package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.OrderDishRequestDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class OrderResponseDto {

    private  Long id;
    private Long idRestaurant;
    private List<OrderDishRequestDto> orderDishes;

}
