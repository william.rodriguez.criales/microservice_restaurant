package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class OrderRequestDto {

    private Long idRestaurant;
    private Long idChef;
    private List<OrderDishRequestDto> orderDishes;




}
