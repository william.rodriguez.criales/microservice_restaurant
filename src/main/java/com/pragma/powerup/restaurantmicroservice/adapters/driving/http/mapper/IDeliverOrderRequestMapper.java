package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DeliverOrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IDeliverOrderRequestMapper {
    @Mapping(target = "id", source = "idOrder")
    @Mapping(target = "status", source = "deliverOrderRequestDto.status")
    Order toOrder (Long idOrder, DeliverOrderRequestDto deliverOrderRequestDto);
}
