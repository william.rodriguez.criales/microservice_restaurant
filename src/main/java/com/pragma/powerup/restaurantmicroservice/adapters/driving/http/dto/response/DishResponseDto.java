package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DishResponseDto {
    private Long id;
    private String name;
    private String idCategory;
    private String description;
    private String price;
    private Long idRestaurant;
    private String urlImage;
    private Boolean active;

}
