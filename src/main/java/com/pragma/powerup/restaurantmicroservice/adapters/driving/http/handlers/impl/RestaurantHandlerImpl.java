package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.impl;


import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.RestaurantResponseDto;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.IRestaurantHandler;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IRestaurantRequestMapper;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper.IRestaurantResponseMapper;

import com.pragma.powerup.restaurantmicroservice.domain.api.IRestaurantServicePort;

import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RestaurantHandlerImpl implements IRestaurantHandler {
    private final IRestaurantServicePort restaurantServicePort;
    private final IRestaurantRequestMapper restaurantRequestMapper;
    private final IRestaurantResponseMapper restaurantResponseMapper;

    @Override
    public RestaurantResponseDto saveRestaurant(RestaurantRequestDto restaurantRequestDto) {
        Restaurant restaurant = restaurantServicePort.saveRestaurant(restaurantRequestMapper.toRestaurant(restaurantRequestDto));
        return restaurantResponseMapper.toRestaurantResponseDto(restaurant);
    }

    @Override
    public Page<RestaurantResponseDto> getAllRestaurant(Pageable pageable) {
        Page<Restaurant> allRestaurant = restaurantServicePort.getAllRestaurant(pageable);
        return allRestaurant.map(restaurant -> restaurantResponseMapper.toRestaurantResponseDto(restaurant));
    }
}
