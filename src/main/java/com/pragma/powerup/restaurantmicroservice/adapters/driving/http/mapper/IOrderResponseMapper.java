package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.mapper;


import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderDishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IOrderResponseMapper {
    @Mapping(source = "orderDishSetter", target = "orderDishes")
    OrderResponseDto toOrderResponseDto(Order order);

    @Mapping(source = "orderDishSetter", target = "orderDishes")
    OrderDishResponseDto toOrderDishResponseDto(Order order);

    }
