package com.pragma.powerup.restaurantmicroservice.adapters.driving.http.controller;

import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.DeliverOrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.request.OrderRequestDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderDishResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import com.pragma.powerup.restaurantmicroservice.adapters.driving.http.handlers.IOrderHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class OrderRestController {

    private final IOrderHandler orderHandler;

    @Operation(summary = "Add a order",
            responses = {
                    @ApiResponse(responseCode = "201", description = "order created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map")))})

    @PostMapping
    public ResponseEntity<OrderResponseDto> makeOrder(@RequestBody OrderRequestDto orderRequestDto) {

        OrderResponseDto orderResponseDto = orderHandler.makeOrder(orderRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderResponseDto);

    }

    @Operation(summary = "List Orders")
    @GetMapping
    //@PageableDefault(sort = {"name"})
    public ResponseEntity<Page<OrderDishResponseDto>> getAllOrders(@RequestParam(required = false) String status, @ParameterObject Pageable pageable) {
        Page<OrderDishResponseDto> orderResponseDtoPage = orderHandler.getAllOrders(pageable, status);
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderResponseDtoPage);
    }


    @Operation(summary = "assign order")
    @PatchMapping("/assign/{id}")
    public ResponseEntity<OrderDishResponseDto> assignOrder(@PathVariable Long id) {
        OrderDishResponseDto orderDishResponseDto = orderHandler.assignOrder(id);
        return ResponseEntity.status(HttpStatus.OK).body(orderDishResponseDto);
    }

    @Operation(summary = "notify order")
    @PatchMapping("/notify/{id}")
    public ResponseEntity<OrderDishResponseDto> notifyOrder(@PathVariable Long id){
        OrderDishResponseDto orderDishResponseDto = orderHandler.notifyOrder(id);
        return ResponseEntity.status((HttpStatus.OK)).body(orderDishResponseDto);
    }

    @Operation(summary = "deliver order")
    @PatchMapping("/deliver/{id}")
    public  ResponseEntity<OrderDishResponseDto> deliverOrder(@RequestParam Long idOrder, @RequestBody DeliverOrderRequestDto deliverOrderRequestDto){
        OrderDishResponseDto orderDishResponseDto = orderHandler.deliverOrder(idOrder, deliverOrderRequestDto);
        return ResponseEntity.status((HttpStatus.OK)).body(orderDishResponseDto);
    }

    @Operation(summary = "List Orders")
    @DeleteMapping("/cancel/{id}")
    public ResponseEntity<String> cancelOrder(@RequestParam Long idOrder) {
         orderHandler.cancelOrder(idOrder);
        return ResponseEntity.status(HttpStatus.OK)
                .body("order canceled");
    }


}
