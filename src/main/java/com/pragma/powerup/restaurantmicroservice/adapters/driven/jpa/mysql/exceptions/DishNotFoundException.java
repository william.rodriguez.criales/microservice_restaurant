package com.pragma.powerup.restaurantmicroservice.adapters.driven.jpa.mysql.exceptions;

public class DishNotFoundException extends Throwable {

    public DishNotFoundException() {
        super();
    }
}
