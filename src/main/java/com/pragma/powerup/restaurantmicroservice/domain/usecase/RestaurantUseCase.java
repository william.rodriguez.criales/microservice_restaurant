package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.configuration.Constants;
import com.pragma.powerup.restaurantmicroservice.domain.api.IRestaurantServicePort;
import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.restaurantmicroservice.feing.UserApi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;



public class RestaurantUseCase implements IRestaurantServicePort {
    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final UserApi userApi;


    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort, UserApi userApi) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.userApi = userApi;

    }

    @Override
    public Restaurant saveRestaurant(Restaurant restaurant) {

        if (isEmpty(restaurant.getName())) {
            throw new ValidationException("empty name");
        } else if (!isNameValid(restaurant.getName())) {
                throw new ValidationException("invalid name");
            }

        if (isEmpty(restaurant.getAddress())) {
            throw new ValidationException("empty address");
        }

        if (isEmpty(restaurant.getIdOwner())) {
            throw new ValidationException("empty id owner");
        } else if (!isOwner(restaurant.getIdOwner())) {
            throw new ValidationException("the user does not owner");
        }

        if (isEmpty(restaurant.getPhone())) {
            throw new ValidationException("empty phone");
        } else if (!isPhoneValid(restaurant.getPhone())) {
            throw new ValidationException("invalid phone");
        }

        if (isEmpty(restaurant.getUrlLogo())) {
            throw new ValidationException("empty url logo");
        }

        if (isEmpty(restaurant.getNit())) {
            throw new ValidationException("empty nit");
        } else if (!isNitValid(restaurant.getNit())) {
            throw new ValidationException("invalid nit");
        }
        Restaurant restaurant1 = restaurantPersistencePort.saveRestaurant(restaurant);
        return restaurant1;
    }

    @Override
    public Page<Restaurant> getAllRestaurant(Pageable pageable) {
        return restaurantPersistencePort.getAllRestaurant(pageable);
    }

    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPhoneValid(String phone) {
        return phone.matches("^[+]?(\\d){7,12}$");

    }

    private boolean isNitValid(String dniNumber) {
        return dniNumber.matches("^(\\d)*$");
    }

    private boolean isEmpty(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return ObjectUtils.isEmpty(field);
    }

    private boolean isNameValid(String name) {
        return  name.matches("^[0-9a-zA-Z]*$")&&!(name.matches("^[\\d]*$"));
    }

    private boolean isOwner(Long idOwner) {
        User user = userApi.getById(idOwner);
        return user.getIdRole().equals(Constants.OWNER_ROLE_ID);


    }

}
