package com.pragma.powerup.restaurantmicroservice.domain.api;

import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IOrderServicePort {

    Order makeOrder(Order order);
    Page<Order> getAllOrders(Pageable pageable, String status);

    Order assignOrder(Long idChef);

    Order notifyOrder(Long id);

    Order deliverOrder(Order order);

    void cancelOrder(Long idOrder);
}
