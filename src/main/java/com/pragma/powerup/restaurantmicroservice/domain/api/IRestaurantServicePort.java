package com.pragma.powerup.restaurantmicroservice.domain.api;

import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IRestaurantServicePort {
    Restaurant saveRestaurant(Restaurant restaurant);
    Page<Restaurant> getAllRestaurant(Pageable pageable);
}
