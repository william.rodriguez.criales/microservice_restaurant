package com.pragma.powerup.restaurantmicroservice.domain.model;

public class OrderDish {



    private Long idOrder;
    private Long idDish;
    private Long quantity;

    public OrderDish(Long idOrder, Long idDish, Long quantity) {
        this.idOrder = idOrder;
        this.idDish = idDish;
        this.quantity = quantity;
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public Long getIdDish() {
        return idDish;
    }

    public void setIdDish(Long idDish) {
        this.idDish = idDish;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
