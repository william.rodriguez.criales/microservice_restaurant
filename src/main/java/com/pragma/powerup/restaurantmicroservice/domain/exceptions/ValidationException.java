package com.pragma.powerup.restaurantmicroservice.domain.exceptions;

public class ValidationException extends RuntimeException{

    public ValidationException(String message) {
        super(message);
    }
}
