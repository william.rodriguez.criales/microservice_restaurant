package com.pragma.powerup.restaurantmicroservice.domain.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class Traceability {
    private Long idOrder;
    private Long idClient;
    private String emailClient;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime date;
    private String statusBack;
    private String statusNew;
    private Long idEmployee;
    private String emailEmployee;

    public Traceability(Long idOrder, Long idClient, String emailClient, LocalDateTime date, String statusBack, String statusNew, Long idEmployee, String emailEmployee) {
        this.idOrder = idOrder;
        this.idClient = idClient;
        this.emailClient = emailClient;
        this.date = date;
        this.statusBack = statusBack;
        this.statusNew = statusNew;
        this.idEmployee = idEmployee;
        this.emailEmployee = emailEmployee;
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public String getEmailClient() {
        return emailClient;
    }

    public void setEmailClient(String emailClient) {
        this.emailClient = emailClient;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatusBack() {
        return statusBack;
    }

    public void setStatusBack(String statusBack) {
        this.statusBack = statusBack;
    }

    public String getStatusNew() {
        return statusNew;
    }

    public void setStatusNew(String statusNew) {
        this.statusNew = statusNew;
    }

    public Long getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Long idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getEmailEmployee() {
        return emailEmployee;
    }

    public void setEmailEmployee(String emailEmployee) {
        this.emailEmployee = emailEmployee;
    }
}
