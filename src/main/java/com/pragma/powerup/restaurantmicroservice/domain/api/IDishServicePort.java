package com.pragma.powerup.restaurantmicroservice.domain.api;

import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IDishServicePort {
    Dish saveDish(Dish dish);

    Dish updateDish(Dish dish);

    Dish activeDish(Long id);

    Page<Dish> getAllDish(Pageable pageable, String idCategory);
}
