package com.pragma.powerup.restaurantmicroservice.domain.spi;

import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IDishPersistencePort {
    Dish saveDish(Dish dish);

    Dish updateDish(Dish dish);

    Dish getById(Long idDish);

    Dish activeDish(Dish dish);

    Page<Dish> getAllDish(Pageable pageable, String idCategory);

    Page<Dish> getAllDish(Pageable pageable);

}
