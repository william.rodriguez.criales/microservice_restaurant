package com.pragma.powerup.restaurantmicroservice.domain.model;

import java.time.LocalDate;
import java.util.List;

public class Order {

    private List<OrderDish> orderDishSetter;
    private Long id;
    private Long idClient;
    private LocalDate date;
    private String status;
    private Long idChef;
    private Long idRestaurant;

    private String pin;

    public Order(List<OrderDish> orderDishSetter, Long id, Long idClient, LocalDate date, String status, Long idChef, Long idRestaurant, String pin) {
        this.orderDishSetter = orderDishSetter;
        this.id = id;
        this.idClient = idClient;
        this.date = date;
        this.status = status;
        this.idChef = idChef;
        this.idRestaurant = idRestaurant;
        this.pin = pin;
    }

    public List<OrderDish> getOrderDishSetter() {
        return orderDishSetter;
    }

    public void setOrderDishSetter(List<OrderDish> orderDishSetter) {
        this.orderDishSetter = orderDishSetter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getIdChef() {
        return idChef;
    }

    public void setIdChef(Long idChef) {
        this.idChef = idChef;
    }

    public Long getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(Long idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}


