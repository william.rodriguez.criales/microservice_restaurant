package com.pragma.powerup.restaurantmicroservice.domain.spi;

import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IOrderPersistencePort {
    Order makeOrder(Order order);

    List<Order> findByIdClientAndStatus(Long idClient, List<String> status);

    Page<Order> getAllOrders(Pageable pageable);

    Page<Order> getAllOrders(Pageable pageable, String status);

    //Page<Order> allotOrder(Order order, Long idChef, Pageable pageable, String status);



    Order getById(Long id);

    Order assignOrder(Order orderId, Long idChef);

    Order notifyOrder(Order order);

    Order deliverOrder(Order order, String status);

    void cancelOrder(Long idOrder);
}
