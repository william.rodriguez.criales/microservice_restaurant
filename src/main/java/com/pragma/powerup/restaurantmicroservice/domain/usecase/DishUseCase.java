package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.configuration.security.jwt.JwtUtils;
import com.pragma.powerup.restaurantmicroservice.domain.api.IDishServicePort;
import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IDishPersistencePort;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IRestaurantPersistencePort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;


public class DishUseCase implements IDishServicePort {
    private final IDishPersistencePort dishPersistencePort;
    private final IRestaurantPersistencePort restaurantPersistencePort;


    public DishUseCase(IDishPersistencePort dishPersistencePort, IRestaurantPersistencePort restaurantPersistencePort) {
        this.dishPersistencePort = dishPersistencePort;
        this.restaurantPersistencePort = restaurantPersistencePort;

    }


    @Override
    public Dish saveDish(Dish dish) {

        if (isEmpty(dish.getName())) {
            throw new ValidationException("empty name");
        }

        if (isEmpty(dish.getIdCategory())) {
            throw new ValidationException("empty id category");
        }

        if (isEmpty(dish.getDescription())) {
            throw new ValidationException("empty description");
        }

        if (isEmpty(dish.getPrice())) {
            throw new ValidationException("empty price");
        } else if (!isPriceValid(dish.getPrice())) {
            throw new ValidationException(("invalid price"));
        }

        if (isEmpty(dish.getIdRestaurant())) {
            throw new ValidationException("empty id restaurant");
        }


        if (isEmpty(dish.getUrlImage())) {
            throw new ValidationException("empty url image");
        }

        if(!validateOwnerRestaurant(dish.getIdRestaurant())){

            throw new ValidationException("the user is not a restaurant owner");

        }


        dish.setActive(true);



        return dishPersistencePort.saveDish(dish);
    }


    @Override
    public Dish updateDish(Dish dish) {
        Dish dishById = dishPersistencePort.getById(dish.getId());
        if (isEmpty(dish.getId())) {
            throw new ValidationException("empty id dish");
        }

        if (!validateOwnerRestaurant(dishById.getIdRestaurant())) {
            throw new ValidationException("the user is not a restaurant owner");
        }

        if (isEmpty(dish.getDescription())) {
            throw new ValidationException("empty description");
        }

        if (isEmpty(dish.getPrice())) {
            throw new ValidationException("empty price");
        } else if (!isPriceValid(dish.getPrice())) {
            throw new ValidationException(("invalid price"));
        }

        return dishPersistencePort.updateDish(dish);
    }

    @Override
    public Dish activeDish(Long id) {
        Dish dish = dishPersistencePort.getById(id);
        if (!validateOwnerRestaurant(dish.getIdRestaurant())) {

            throw new ValidationException("the user is not a restaurant owner");

        }

        return dishPersistencePort.activeDish(dish);
    }

    @Override
    public Page<Dish> getAllDish(Pageable pageable, String idCategory) {
        if (idCategory==null){
          return dishPersistencePort.getAllDish(pageable);
        }
        return dishPersistencePort.getAllDish(pageable, idCategory);
    }

    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPriceValid(String price) {
        return price.matches("^[\\d]*$") && !price.matches("^[0]*$");

    }

    private boolean isEmpty(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return ObjectUtils.isEmpty(field);
    }

    private Boolean validateOwnerRestaurant(Long idRestaurant) {
        Restaurant restaurant = restaurantPersistencePort.getById(idRestaurant);
        if (restaurant.getIdOwner() == getIdOwnerFromToken()) {
            return true;
        }
        return false;
    }

    private Long getIdOwnerFromToken() {

        return Long.parseLong(JwtUtils.getClaim("idUser"));
    }



}
