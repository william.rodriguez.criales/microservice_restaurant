package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.configuration.security.jwt.JwtUtils;
import com.pragma.powerup.restaurantmicroservice.domain.api.IOrderServicePort;
import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import com.pragma.powerup.restaurantmicroservice.domain.model.Traceability;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IDishPersistencePort;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.restaurantmicroservice.feing.MessengerApi;
import com.pragma.powerup.restaurantmicroservice.feing.TraceabilityApi;
import com.pragma.powerup.restaurantmicroservice.feing.UserApi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class OrderUseCase implements IOrderServicePort {

    private final IOrderPersistencePort orderPersistencePort;
    private final IDishPersistencePort dishPersistencePort;
    private final MessengerApi messengerApi;
    private final UserApi userApi;
    private final TraceabilityApi traceabilityApi;


    public OrderUseCase(IOrderPersistencePort orderPersistencePort, IDishPersistencePort dishPersistencePort, MessengerApi messengerApi, UserApi userApi, TraceabilityApi traceabilityApi) {
        this.orderPersistencePort = orderPersistencePort;
        this.dishPersistencePort = dishPersistencePort;
        this.messengerApi = messengerApi;
        this.userApi = userApi;
        this.traceabilityApi = traceabilityApi;
    }

    @Override
    public Order makeOrder(Order order) {

        if (isEmpty(order.getIdRestaurant())) {
            throw new ValidationException("empty id restaurant");
        }

        if (isEmpty(order.getIdChef())) {
            throw new ValidationException("empty id chef");
        }

        for (int i = 0; i < order.getOrderDishSetter().size(); i++) {
            if (!validateDishRestaurant(order.getIdRestaurant(), order.getOrderDishSetter().get(i).getIdDish())) {
                throw new ValidationException("the dish whit id " + order.getOrderDishSetter().get(i).getIdDish() +
                        " does not belong to the restaurant whit id " + order.getIdRestaurant());
            }
        }

        order.setIdClient(Long.parseLong(JwtUtils.getClaim("idUser")));

        if (!isOrderValid(order.getIdClient())) {
            throw new ValidationException("the client have a order earring");
        }

        order.setDate(LocalDate.now());
        order.setStatus("pendiente");
        Order orderSaved = orderPersistencePort.makeOrder(order);
        saveTraceability(orderSaved, "", null);
        return orderSaved;

    }


    @Override
    public Page<Order> getAllOrders(Pageable pageable, String status) {

        if (status == null) {
            return orderPersistencePort.getAllOrders(pageable);
        }
        return orderPersistencePort.getAllOrders(pageable, status);
    }

    @Override
    public Order assignOrder(Long id) {

        Long idChef = Long.parseLong(JwtUtils.getClaim("idUser"));
        if (id == null) {
            throw new ValidationException("the id is null, please insert a valid id ");
        }
        User employee = userApi.getById(idChef);
        Order orderPreviousById = orderPersistencePort.getById(id);
        String previousStatus = orderPreviousById.getStatus();
        Order orderSaved = orderPersistencePort.assignOrder(orderPreviousById, idChef);
        saveTraceability(orderSaved, previousStatus, employee);
        return orderSaved;
    }

    @Override
    public Order notifyOrder(Long id) {

        Random random = new Random();
        int randomWithNextInt = random.nextInt(0, 1000);
        String randomString = randomWithNextInt + "";
        if (id == null) {

            throw new ValidationException("the id is null, please insert a valid id ");
        }
        Long idEmployee = Long.parseLong(JwtUtils.getClaim("idUser"));
        User employee = userApi.getById(idEmployee);
        Order orderPreviousById = orderPersistencePort.getById(id);
        User user = userApi.getById(orderPreviousById.getIdClient());
        messengerApi.sendMessage(user.getName(), user.getPhone(), randomString);
        String previousStatus = orderPreviousById.getStatus();
        Order orderSaved = orderPersistencePort.notifyOrder(orderPreviousById);
        saveTraceability(orderSaved, previousStatus, employee);

        return orderSaved;

    }

    @Override
    public Order deliverOrder(Order order) {

        if (order.getId() == null) {
            throw new ValidationException("the id is null, please insert a valid id ");
        }

        if (isEmpty(order.getPin())) {
            throw new ValidationException("the pin is null, please insert a pin ");
        }

        if (!isPinValid(order.getPin())) {
            throw new ValidationException("the pin is invalid, please insert a pin of 3 numbers ");
        }

        if (isEmpty(order.getStatus())) {
            throw new ValidationException("the status is null, please insert the status deliver");

        }

        Order orderById = orderPersistencePort.getById(order.getId());

        if (orderById.getStatus().equals("entregado")) {
            throw new ValidationException(("the order has already been delivered"));
        }

        if (!order.getStatus().equals("entregado")) {
            throw new ValidationException(("please insert the status deliver - entregado"));

        }

        if (!orderById.getStatus().equals("listo")) {
            throw new ValidationException(("the order is not could deliver, does not list"));
        }

        Long idEmployee = Long.parseLong(JwtUtils.getClaim("idUser"));
        User employee = userApi.getById(idEmployee);
        String previousStatus = orderById.getStatus();
        Order orderSaved = orderPersistencePort.deliverOrder(orderById, order.getStatus());
        saveTraceability(orderSaved, previousStatus, employee);

        return orderSaved;
    }

    @Override
    public void cancelOrder(Long idOrder) {

        if (idOrder == null) {
            throw new ValidationException("the id order is null, please insert a valid id ");
        }
        Order order = orderPersistencePort.getById(idOrder);
        Long idClient = Long.parseLong(JwtUtils.getClaim("idUser"));

        if (!idClient.equals(order.getIdClient())) {
            throw new ValidationException("the order to cancel is not your order");
        }

        if (!order.getStatus().equals("pendiente")) {
            throw new ValidationException("sorry, your order is in make and don´t could is cancel");

        }

        Long idEmployee = Long.parseLong(JwtUtils.getClaim("idUser"));
        User employee = userApi.getById(idEmployee);

        orderPersistencePort.cancelOrder(idOrder);

        String previousStatus = order.getStatus();
        order.setStatus("canceled");

        saveTraceability(order, previousStatus, employee);

    }

    private boolean isOrderValid(Long idClient) {
        List<String> status = List.of("pendiente", "en_preparacion", "listo");
        List<Order> orderById = orderPersistencePort.findByIdClientAndStatus(idClient, status);
        if (!orderById.isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean isEmpty(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return ObjectUtils.isEmpty(field);
    }

    private boolean validateDishRestaurant(Long idRestaurant, Long idDish) {
        Dish dish = dishPersistencePort.getById(idDish);
        return dish.getIdRestaurant() == idRestaurant;
    }

    private boolean isPinValid(String pin) {
        return pin.matches("^(\\d){3}$");
    }

    private void saveTraceability(Order order, String previousStatus, User employee) {
        User user = userApi.getById(order.getIdClient());
        LocalDateTime date = LocalDateTime.now();
        Long defaultIdEmployee = 0L;
        String defaultEmailEmployee = "";

        if (employee!=null){

            defaultIdEmployee = employee.getId();
            defaultEmailEmployee = employee.getMail();

        }

        Traceability traceability = new Traceability(order.getId(), order.getIdClient(),
                user.getMail(), date,previousStatus,
                order.getStatus(),defaultIdEmployee, defaultEmailEmployee );

        traceabilityApi.saveTraceability(traceability);


    }
}

