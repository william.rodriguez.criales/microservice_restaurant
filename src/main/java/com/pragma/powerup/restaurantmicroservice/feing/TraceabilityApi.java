package com.pragma.powerup.restaurantmicroservice.feing;

import com.pragma.powerup.restaurantmicroservice.configuration.security.FeignAuthenticationConfig;
import com.pragma.powerup.restaurantmicroservice.domain.model.Traceability;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "traceability", url = "${client.traceability.baseUrl}",configuration = FeignAuthenticationConfig.class)
public interface TraceabilityApi {

        @PostMapping()
        Traceability saveTraceability(@RequestBody Traceability traceability);
}
