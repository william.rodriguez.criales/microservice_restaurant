package com.pragma.powerup.restaurantmicroservice.feing;

import com.pragma.powerup.restaurantmicroservice.configuration.security.FeignAuthenticationConfig;
import com.pragma.powerup.restaurantmicroservice.domain.model.Message;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "message", url = "${client.message.baseUrl}",configuration = FeignAuthenticationConfig.class)
public interface MessengerApi {

        @PostMapping()
        Message sendMessage(@RequestParam("name") String name, @RequestParam("phone") String phone,@RequestParam("pin") String pin);
}
