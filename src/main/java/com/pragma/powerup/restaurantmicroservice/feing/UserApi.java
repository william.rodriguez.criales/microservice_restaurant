package com.pragma.powerup.restaurantmicroservice.feing;


import com.pragma.powerup.restaurantmicroservice.configuration.security.FeignAuthenticationConfig;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user", url = "${client.user.baseUrl}",configuration = FeignAuthenticationConfig.class)
public interface UserApi {
    @GetMapping("/{id}")
    User getById(@PathVariable Long id);
}

