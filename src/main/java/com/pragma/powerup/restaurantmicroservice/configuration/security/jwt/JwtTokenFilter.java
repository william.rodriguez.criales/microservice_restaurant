package com.pragma.powerup.restaurantmicroservice.configuration.security.jwt;


import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jose.jws.JwsAlgorithms;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.pragma.powerup.restaurantmicroservice.configuration.security.jwt.JwtProvider.ROLES;

public class JwtTokenFilter extends OncePerRequestFilter {


    @Autowired
    JwtProvider jwtProvider;
    private List<String> excludedPrefixes = Arrays.asList("/swagger-ui/**", "/actuator/**");
    private AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain)
            throws ServletException, IOException {


        String token = getToken(req);
        if (token != null && jwtProvider.validateToken(token)) {
            Claims claims = jwtProvider.getClaims(token);
            List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(String.valueOf( claims.get(ROLES, Long.class))));
            //UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);

            Jwt jwt = Jwt.withTokenValue(token)
                    .header("alg", JwsAlgorithms.RS256)
                    .claim("idUser", claims.get("idUser"))
                    .expiresAt(claims.getExpiration().toInstant())
                    .issuedAt(claims.getIssuedAt().toInstant())
                    .build();
            JwtAuthenticationToken auth = new JwtAuthenticationToken(jwt, authorities);
            SecurityContextHolder.getContext().setAuthentication(auth);
            //SecurityContext emptyContext = SecurityContextHolder.createEmptyContext();
            //emptyContext.setAuthentication(auth);
            //SecurityContextHolder.setContext(emptyContext);



        }
        filterChain.doFilter(req, res);

    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String currentRoute = request.getServletPath();
        for (String prefix : excludedPrefixes) {
            if (pathMatcher.matchStart(prefix, currentRoute)) {
                return true;
            }
        }
        return false;
    }

    private String getToken(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            return header.substring(7); // return everything after "Bearer "
        }
        return null;
    }
}
