package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.configuration.security.jwt.JwtUtils;
import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IDishPersistencePort;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IRestaurantPersistencePort;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
//@ExtendWith(MockitoExtension.class)
class DishUseCaseTest {

    @Mock
    IDishPersistencePort iDishPersistencePort;
    @Mock
    IRestaurantPersistencePort iRestaurantPersistencePort;


    @InjectMocks
    DishUseCase dishUseCase;


    @BeforeAll
    static void setup() {
        Mockito.mockStatic(JwtUtils.class);
    }


    @ParameterizedTest
    @MethodSource("invalidRequest")
    void saveDishValidations(Dish dish, String message) {
        //arrange

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.saveDish(dish));
        //assert
        assertEquals(message, validationException.getMessage());
        verify(iDishPersistencePort, never()).saveDish(dish);
    }

    static Stream<Arguments> invalidRequest() {
        return Stream.of(Arguments.of(new Dish(1L, "", "", "", "", null, "", null),
                        "empty name"),
                Arguments.of(new Dish(1L, "pollo", "", "", "777", null, "", null),
                        "empty id category"),
                Arguments.of(new Dish(1L, "pollo", "1", "", "777", null, "", null),
                        "empty description"),
                Arguments.of(new Dish(1L, "pollo", "1", "almuerzo", "", null, "", null),
                        "empty price"),
                Arguments.of(new Dish(1L, "pollo", "1", "almuerzo", "0", null, "", null),
                        "invalid price"),
                Arguments.of(new Dish(1L, "pollo", "1", "almuerzo", "1", null, "", null),
                        "empty id restaurant"),
                Arguments.of(new Dish(1L, "pollo", "1", "almuerzo", "1", 1L, "", null),
                        "empty url image")

        );

    }

    @Test
    void saveDishSuccessfully() {

        //arrange
        Dish dish = new Dish(1L, "pollo", "1", "almuerzo", "1", 1L, "www", true);
        Restaurant restaurant = new Restaurant(1L, "restau", "1", 2L, "111111111", "www", "www");
        String idUserFromToken = "2";
        //when
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(iDishPersistencePort.saveDish(any(Dish.class))).thenReturn(dish);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);

        //act
        dishUseCase.saveDish(dish);
        //assert
        verify(iDishPersistencePort, times(1)).saveDish(dish);

    }

    @Test
    void updateDishValidationsEmptyId() {
        //arrange
        Dish dish = new Dish(null, "", "", "", "", null, "", null);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.updateDish(dish));
        //assert
        assertEquals("empty id dish", validationException.getMessage());
        verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void updateDishValidationsInvalidOwnerRestaurant() {
        //arrange
        Dish dish = new Dish(1L, "name", "category", "description", "888", 1L, "", null);
        Restaurant restaurant = new Restaurant(1L, null, null, 1L, null, null, null);
        String idUserFromToken = "2";
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.updateDish(dish));
        //assert
        assertEquals("the user is not a restaurant owner", validationException.getMessage());
        verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void updateDishValidationsEmptyDescription() {
        //arrange
        Dish dish = new Dish(1L, "name", "category", "", "888", 1L, "", null);
        Restaurant restaurant = new Restaurant(1L, null, null, 1L, null, null, null);
        String idUserFromToken = "1";
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.updateDish(dish));
        //assert
        assertEquals("empty description", validationException.getMessage());
        verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void updateDishValidationsEmptyPrice() {
        //arrange
        Dish dish = new Dish(1L, "name", "category", "plato", "", 1L, "", null);
        Restaurant restaurant = new Restaurant(1L, null, null, 1L, null, null, null);
        String idUserFromToken = "1";
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.updateDish(dish));
        //assert
        assertEquals("empty price", validationException.getMessage());
        verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void updateDishValidationsInvalidPrice() {
        //arrange
        Dish dish = new Dish(1L, "name", "category", "plato", "-8", 1L, "", null);
        Restaurant restaurant = new Restaurant(1L, null, null, 1L, null, null, null);
        String idUserFromToken = "1";
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> dishUseCase.updateDish(dish));
        //assert
        assertEquals("invalid price", validationException.getMessage());
        verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void updateDishValidationsSuccessful() {
        //arrange
        Dish dish = new Dish(1L, "name", "category", "plato", "8", 1L, "", null);
        Restaurant restaurant = new Restaurant(1L, null, null, 1L, null, null, null);
        String idUserFromToken = "1";
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iRestaurantPersistencePort.getById(anyLong())).thenReturn(restaurant);
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        //act
        dishUseCase.updateDish(dish);
        //assert
        verify(iDishPersistencePort, times(1)).updateDish(dish);
    }


}
