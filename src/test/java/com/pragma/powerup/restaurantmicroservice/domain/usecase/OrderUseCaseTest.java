package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.configuration.security.jwt.JwtUtils;
import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Dish;
import com.pragma.powerup.restaurantmicroservice.domain.model.Message;
import com.pragma.powerup.restaurantmicroservice.domain.model.Order;
import com.pragma.powerup.restaurantmicroservice.domain.model.OrderDish;
import com.pragma.powerup.restaurantmicroservice.domain.model.Traceability;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IDishPersistencePort;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.restaurantmicroservice.feing.MessengerApi;
import com.pragma.powerup.restaurantmicroservice.feing.UserApi;
import org.junit.jupiter.api.BeforeAll;
import com.pragma.powerup.restaurantmicroservice.feing.TraceabilityApi;
import com.pragma.powerup.restaurantmicroservice.feing.UserApi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class OrderUseCaseTest {

    @Mock
    IOrderPersistencePort iOrderPersistencePort;
    @Mock
    IDishPersistencePort iDishPersistencePort;
    @Mock
    UserApi userApi;
    @Mock
    MessengerApi messengerApi;


    @Mock
    TraceabilityApi traceabilityApi;

    @BeforeAll
    static void setup() {
        Mockito.mockStatic(JwtUtils.class);
    }

    @InjectMocks
    OrderUseCase orderUseCase;
    @InjectMocks
    DishUseCase dishUseCase;


    @Test
    void makeOrderValidationsEmptyIdRestaurant() {
        //arrange
        Order order = new Order(null, null, null, LocalDate.now(), "", null, null, null);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.makeOrder(order));
        //assert
        assertEquals("empty id restaurant", validationException.getMessage());
        verify(iOrderPersistencePort, never()).makeOrder(order);
    }

    @Test
    void makeOrderValidationsEmptyIdChef() {
        //arrange
        Order order = new Order(null, null, null, LocalDate.now(), "", null, 1L, null);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.makeOrder(order));
        //assert
        assertEquals("empty id chef", validationException.getMessage());
        verify(iOrderPersistencePort, never()).makeOrder(order);
    }

    @Test
    void makeOrderValidationsInvalidDishOfRestaurant() {
        //arrange
        List<OrderDish> orderDishList = Collections.singletonList(new OrderDish(1L, 1L, 3L));
        Order order = new Order(orderDishList, 1L, 3L, LocalDate.now(), "yyyy", 1L, 1L, null);
        Dish dish = new Dish(1L, "Pollo", "1L", "Almuerzo", "300000", 2L, "wwww", true);
        //when
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iOrderPersistencePort.getById(anyLong())).thenReturn(order);

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.makeOrder(order));
        //assert
        assertEquals("the dish whit id 1 does not belong to the restaurant whit id 1", validationException.getMessage());
        verify(iOrderPersistencePort, never()).makeOrder(order);
    }

    @Test
    void makeOrderValidationsInvalidOrder() {
        //arrange
        List<OrderDish> orderDishList = Collections.singletonList(new OrderDish(1L, 1L, 3L));
        Order order = new Order(orderDishList, 1L, 3L, LocalDate.now(), "yyyy", 1L, 1L, null);
        Dish dish = new Dish(1L, "Pollo", "1L", "Almuerzo", "300000", 1L, "wwww", true);
        List<Order> orderList = Collections.singletonList(new Order(orderDishList, 1L, null, LocalDate.now(), "pendiente", 1L, 1L, null));
        String idUserFromTokenString = "3";
        //when
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromTokenString);
        when(iOrderPersistencePort.findByIdClientAndStatus(anyLong(), anyList())).thenReturn(orderList);
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iOrderPersistencePort.getById(anyLong())).thenReturn(order);

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.makeOrder(order));
        //assert
        assertEquals("the client have a order earring", validationException.getMessage());
        verify(iOrderPersistencePort, never()).makeOrder(order);
    }

    @Test
    void makeOrderValidationsSuccessful() {
        //arrange
        List<OrderDish> orderDishList = Collections.singletonList(new OrderDish(1L, 1L, 3L));
        Order order = new Order(orderDishList, 1L, 3L, LocalDate.now(), "pendiente", 1L, 1L, null);
        Dish dish = new Dish(1L, "Pollo", "1L", "Almuerzo", "300000", 1L, "wwww", true);
        String idUserFromTokenString = "3";
        User user = new User(1L,"www","rrr","11111", "33333333",LocalDate.now().minusYears(19L),"w@gmail.com","9999",1L);
        Traceability traceability = new Traceability(1L,1L,"w@gmail", LocalDateTime.now(),"","",1L,"");

        //when
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromTokenString);
        when(iDishPersistencePort.getById(anyLong())).thenReturn(dish);
        when(iOrderPersistencePort.getById(anyLong())).thenReturn(order);
        when(iOrderPersistencePort.makeOrder(any(Order.class))).thenReturn(order);
        when(iOrderPersistencePort.findByIdClientAndStatus(anyLong(), anyList())).thenReturn(Collections.emptyList());
        when(userApi.getById(anyLong())).thenReturn(user);
        when(traceabilityApi.saveTraceability(any(Traceability.class))).thenReturn(traceability);
        //act
        orderUseCase.makeOrder(order);
        //assert
        verify(iOrderPersistencePort, times(1)).makeOrder(order);
    }


    @Test
    void assignOrderValidationsEmptyId() {
        //arrange
        Order order = new Order(null, null, null, LocalDate.now(), "", null, 1L, null);
        Long idOrder = null;
        String idUserFromToken = "2";
        //when
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        when(iOrderPersistencePort.getById(anyLong())).thenReturn(order);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.assignOrder(idOrder));
        //assert
        assertEquals("the id is null, please insert a valid id ", validationException.getMessage());
        verify(iOrderPersistencePort, never()).assignOrder(order, idOrder);
    }


    @Test
    void notifyOrderValidationsEmptyIdOrder() {
        //arrange
        Order order = new Order(null, null, null, LocalDate.now(), "", null, 1L, null);
        Long idOrder = null;
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> orderUseCase.notifyOrder(idOrder));
        //assert
        assertEquals("the id is null, please insert a valid id ", validationException.getMessage());
        verify(iOrderPersistencePort, never()).notifyOrder(order);
    }

    @Test
    void notifyOrderSuccessful() {
        //arrange
        Order order = new Order(null, 1L, null, LocalDate.now(), "", null, 1L, null);
        User user = new User(1L, "www", "rrr", "1111", "3333333", LocalDate.now(), "w@gmail.com", "1111", 1L);
        Long idOrder = 1L;
        String idUserFromToken = "2";
        Message message = new Message("ww","+5733333333");
        //when
        //TODO: terminar test successful
        when(JwtUtils.getClaim(anyString())).thenReturn(idUserFromToken);
        when(iOrderPersistencePort.getById(anyLong())).thenReturn(order);
        when(userApi.getById(anyLong())).thenReturn(user);
        when(messengerApi.sendMessage(anyString(), anyString(), anyString())).thenReturn(message);
        //act
        orderUseCase.notifyOrder(idOrder);
        //assert
        verify(iOrderPersistencePort, never()).notifyOrder(order);
    }
}
