package com.pragma.powerup.restaurantmicroservice.domain.usecase;

import com.pragma.powerup.restaurantmicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.restaurantmicroservice.domain.model.Restaurant;
import com.pragma.powerup.restaurantmicroservice.domain.model.User;
import com.pragma.powerup.restaurantmicroservice.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.restaurantmicroservice.feing.UserApi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantUseCaseTest {

    @Mock
    IRestaurantPersistencePort iRestaurantPersistencePort;
    @Mock
    UserApi userApi;
    @InjectMocks
    RestaurantUseCase restaurantUseCase;

/*
    @ParameterizedTest
    @MethodSource("invalidRequest")
    void saveRestaurantValidations(Restaurant restaurant, String message) {
        //arrange

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals(message, validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);
    }

    static Stream<Arguments> invalidRequest() {
        return Stream.of(Arguments.of(new Restaurant(1L, "", "", null, "", "", ""),
                        "empty name"),
                Arguments.of(new Restaurant(1L, "1111", "", null, "", "", ""),
                        "invalid name"),
                Arguments.of(new Restaurant(1L, "restaurant1", "", null, "", "", ""),
                        "empty address"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", null, "", "", ""),
                        "empty id owner"),
                //TODO:test user owner
                //Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 1L, "", "", "", "", "", "", null),
                //        "the user does not owner"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 4L, "", "", ""),
                        "empty phone"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 4L, "111111", "", ""),
                        "invalid phone"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 4L, "11111111", "", ""),
                        "empty url logo"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 4L, "11111111", "11111", ""),
                        "empty nit"),
                Arguments.of(new Restaurant(1L, "restaurant1", "cll 9", 4L, "11111111", "11111", "aaa"),
                        "invalid nit")

        );


    }

 */

    @Test
    void saveRestaurantValidationsEmptyName() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "", "", null, "", null, "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty name", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);
    }

    @Test
    void saveRestaurantValidationsInvalidName() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "2222", "", null, "", null, "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("invalid name", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);
    }

    @Test
    void saveRestaurantValidationsEmptyAddress() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "", null, "", null, "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty address", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);
    }

    @Test
    void saveRestaurantValidationsEmptyIdOwner() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", null, "", null, "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty id owner", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);
    }

    @Test
    void saveRestaurantValidationsInvalidIdOwner() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 2L, "", null, "");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 2L);

        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("the user does not owner", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantValidationsEmptyPhone() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 4L, "", null, "");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty phone", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantValidationsInvalidPhone() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 4L, "33333", null, "");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("invalid phone", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantValidationsEmptyUrlLogo() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 4L, "+57333333333", null, "");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty url logo", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantValidationsEmptyNit() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 4L, "+57333333333", "www.logo.com", "");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("empty nit", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantValidationsInvalidNit() {
        //arrange
        Restaurant restaurant = new Restaurant(null, "restaurant", "cll 8 # 7-15", 4L, "+57333333333", "www.logo.com", "rrrr");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> restaurantUseCase.saveRestaurant(restaurant));
        //assert
        assertEquals("invalid nit", validationException.getMessage());
        verify(iRestaurantPersistencePort, never()).saveRestaurant(restaurant);

    }

    @Test
    void saveRestaurantSuccessfully() {

        //arrange
        Restaurant restaurant = new Restaurant(1L, "name", "cll 9", 4L, "+571111111", "1111", "11111");
        User user = new User(1L,"will","alll", "11111", "11111111", LocalDate.now().minusYears(19L), "ww@gmail.com", "1111", 4L);
        //when
        when(iRestaurantPersistencePort.saveRestaurant(any(Restaurant.class))).thenReturn(restaurant);
        when(userApi.getById(anyLong())).thenReturn(user);
        //act
        restaurantUseCase.saveRestaurant(restaurant);
        //assert
        verify(iRestaurantPersistencePort, times(1)).saveRestaurant(restaurant);
    }
}