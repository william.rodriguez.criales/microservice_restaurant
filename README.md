<br />
<div align="center">
<h3 align="center">Microservice restaurant by William Rodriguez</h3>
  <p align="center">
    In this challenge you are going to design the backend of a microservice to creation of restaurant.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation


1. Clone the follow repository
   ```sh
   https://gitlab.com/william.rodriguez.criales/microservice_restaurant.git
   ```
2. Create a new database in MySQL running this script
   ```
   mysql -u root -p plazoleta < src/main/resources/dbschema.sql
   ```
3. Update the database connection settings
   ```yml
   # src/main/resources/application-dev.yml
   spring:
      datasource:
          url: jdbc:mysql://localhost/plazoleta
          username: root
          password: <your-password>
   ```
4. Open Swagger UI 

<!-- USAGE -->
## Usage

1. Right-click the class RestaurantMicroserviceApplication and choose Run
2. Open [http://localhost:8090/swagger-ui/index.html](http://localhost:8090/swagger-ui/index.html) in your web browser
3. execute the endpoint "restaurant-rest-controller" to create a restaurant"
4. execute the endpoint "dish-rest-controller"/POST/dish/ to create a dish", the restaurant could exist to create dish
5. execute the endpoint "dish-rest-controller"/PATCH/dish/{id}/ to change price and description to existing dish
<!-- ROADMAP -->
## Tests

- Right-click the test folder and choose Run tests with coverage
